import requests
import pytest

# List of URLs to test
urls_to_monitor = [
    "http://example.com",
    "https://google.com",
    "http://nonexistenturl.com"
]

# Define the test function
@pytest.mark.parametrize("url", urls_to_monitor)
def test_http_site_is_up(url):
    response = requests.get(url)
    assert response.status_code == 200, f"{url} is not accessible"
